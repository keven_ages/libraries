# Checkboxes module #

A module for selecting / deselecting checkboxes, either by parent DOM element or by a custom array of checkbox elements

# Basic Usage #

```html
<script src="../src/Checkboxes.js"></script>

<button id="select_all">Select/Deselect All</button>
<button id="select_first_and_second">Select/Deselect 1st and 3rd</button>

<div id="group_cities">
	<label for="frm_cities_0">
		<input type="checkbox" id="frm_cities_0" name="cities[0]" value="1">
		Toronto
	</label>
	<label for="frm_cities_1">
		<input type="checkbox" id="frm_cities_1" name="cities[1]" value="2">
		Winnipeg
	</label>
	<label for="frm_cities_2">
		<input type="checkbox" id="frm_cities_2" name="cities[2]" value="3">
		Vancouver
	</label>
</div>
```

Dom element containing checkboxes one using an anonymous callback, another without


```javascript

var citiesSelected = false;

document.getElementById('select_all').onclick = function( e ){
		
		e.preventDefault();

		if(citiesSelected){
			
			Checkboxes.check('group_cities', 'none', function() {
				citiesSelected = false;
			});
			
		} else {

			Checkboxes.check('group_cities', 'all', function(){
				citiesSelected = true;
			});
		
		}
	}
```

Array of checkboxes

```javascript

var customSelected = false;
var customArrayOfCheckboxes = ['frm_cities_0', 'frm_cities_2'];

document.getElementById('select_first_and_third').onclick = function( e ) {
		
		e.preventDefault();

		if(customSelected){
			
			Checkboxes.check(customArrayOfCheckboxes, 'none', function(){
				customSelected = false;
			});

		} else {
			
			Checkboxes.check(customArrayOfCheckboxes, 'all', function(){
				customSelected = true;
			});

		}
	}
```

# Examples #

Please see examples/all.html