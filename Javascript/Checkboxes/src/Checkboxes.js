var Checkboxes = (function () {
  "use strict";
  // private
  var checkboxes = [];
  // change a single checkbox element's state: bool (true/false)
  function changeCheckState ( state ) {
    for ( var i = 0; i < checkboxes.length; i++ ){
      checkboxes[i].checked = state; 
    }
    return false;
  }    
  // fill the checkboxes array with checkbox elements from a parent DOM element or an array of DOM.ids
  function setCheckboxes ( ele ){
    var c;
    //reset the var on each call
    checkboxes = [];
    //element DOM ID containing checkboxes
    if ( typeof ele === 'string' ){
      c = document.getElementById( ele ).getElementsByTagName("input");
    // custom array of checkboxes
    // @see checkboxesById()
    } else if ( ele instanceof Array ) {
      checkboxes = checkboxesById( ele );
      return true;
    //Throw an error if the conditions aren't met.  No point in continuing on.
    } else {
      throw "Invalid first parameter - Must be one of: Parent DOM element ID|Array";
    }
    
    for ( var i = 0; i < c.length; i++ ) {
      if( c[i].type === 'checkbox' ){ checkboxes.push(c[i]); }
    }

    return false;
  }
  // DOM.ids for array of checkboxes to change
  function checkboxesById( ele ) {
    var c = [];
    for ( var i = 0; i < ele.length; i++ ){
      c.push(document.getElementById(ele[i]));
    }
    return c;
  }
  // public API
  return { 

    check: function ( ele, state, callback ){
        //Try/Catch for errors to console
        try {

          setCheckboxes ( ele );
          
          if(state === 'all'){
            changeCheckState(true);
          } else if (state == 'none') {
            changeCheckState(false);
          } else {
            throw "Invalid second parameter - Must be one of:  all|none";
          }

          if(typeof callback === 'function'){
            callback();
          }
        //Throw exceptions to the console  
        } catch (e) {
          console.log(e);
        }
    }

  };

}());